const webpack = require('webpack');
const path = require('path');

module.exports = env => ({
  target: 'web',
  entry: ["babel-polyfill", "./src/index"],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  node: {
    fs: "empty"
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ["env"]
          }
        }
      }
    ]
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      os: env.os
    })
  ],
});