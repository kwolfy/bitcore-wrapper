require('../src');

const {getWallet} = require('./wallets');

const aliceData = require('fs').readFileSync(__dirname + '/alice.json', 'utf8');
const bobAddress = 'moTW3woZN6VZs1vPzLjg1Ssn1zdw4e4wbC';

const api = createApiFromData(aliceData);
const bob = createApiFromData(getWallet(bobAddress));

sendMoney(api);

function sendMoney(api) {
  let opts = {
    toAddress: bobAddress,
    amount: 100000,
    message: 'test message'
  };

  api.getMainAddresses((err, history) => {
    console.log('fee', err, history);
  });

}