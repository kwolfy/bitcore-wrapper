require('../src');

const {setWallet} = require('./wallets');

global.TestNet = false;

createApiWithNewWallet((err, api) => {

  api.createAddress((err, address) => {
    console.log('create address', err, address);

    setWallet(address.address, api.export());
  });

});

