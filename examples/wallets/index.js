const fs = require('fs');
const path = require('path');

exports.getWallet = address => fs.readFileSync(path.join(__dirname, address), 'utf8');
exports.setWallet = (address, data) => fs.writeFileSync(path.join(__dirname, address), JSON.stringify(data));