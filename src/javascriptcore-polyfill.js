global.crypto = { getRandomValues: require('polyfill-crypto.getrandomvalues') };

if(process.env.os === 'android') {
  global.XMLHttpRequest = require('./polifills/xmlhttprequest-android');
}
else if(process.env.os === 'ios') {

}
else {
  //for tests
  global.XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
}