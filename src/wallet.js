const BitcoreWallet = require('./bitcore-wallet');

const baseUrl = 'https://bws.bitpay.com/bws/api';

class Wallet {
  constructor(opts) {
    this.api = new BitcoreWallet({baseUrl});
  }

  importFromMnemonic(words, {passphase}, cb) {
    console.log({network: this._getNetwork(), passphase})
    this.api.importFromMnemonic(words, {network: this._getNetwork(), passphase}, cb);
  }

  importFromData(data) {
    this.api.import(data);
  }

  createNew({}, cb) {
    this.api.seedFromRandomWithMnemonic({network: this._getNetwork()});
    this.api.createWallet(
      "Eleet Wallet", "Anonymous", 1, 1,
      {network: this._getNetwork()},
      cb);
  }

  open(cb) {
    this.api.openWallet(cb);
  }

  _getNetwork() {
    return global.TestNet ? 'testnet' : 'livenet';
  }

  export() {
    return this.api.export();
  }

  exportMnemonic() {
    return this.api.getMnemonic();
  }

  createAddress(cb) {
    this.api.createAddress({}, cb);
  }

  getMainAddresses(cb) {
    this.api.getMainAddresses({}, cb);
  }

  getBalance(cb) {
    return this.api.getBalance({}, cb);
  }

  subscribeTxConfirmation(txId, cb) {
    this.api.txConfirmationSubscribe(txId, cb);
  }

  unsubscribeTxConfirmation(txId, cb) {
    this.api.txConfirmationUnsubscribe(txId, cb);
  }

  /*
  {
    version: '1.0.0',
    createdOn: 1506103459,
    id: '015061034599970000',
    type: 'NewIncomingTx',
    data:
     { txid: '3ff5c36d22d3fbd362a60c4a18c4c6303048e91f15ee46e8c30e27af8aca3694',
       address: 'mhN5NDKJxWMeton2swpCCB3MavEsc8GvTa',
       amount: 30000 },
    walletId: '380b2efa-e05e-4e50-a19e-46c6033de45f',
    creatorId: null
  }
   */

  onNotification(cb, delaySeconds = 1) {
    this.api.setNotificationsInterval(delaySeconds);
    this.api.on('notification', cb);
  }

  getTx(txId, cb) {
    return this.api.getTx(txId, cb);
  }

  getStatusByIdentifier(identifier, includeExtendedInfo, cb) {
    console.log({identifier, includeExtendedInfo});
    return this.api.getStatusByIdentifier({identifier, includeExtendedInfo}, cb);
  }

  getHistory(opts, cb) {
    this.api.getTxHistory(opts, cb);
  }

  sendMoney(opts, cb) {
    this._sendMoney(opts)
      .then(result => cb(null, result))
      .catch(cb);
  }

  getSendMaxInfo(opts, cb) {
    this.api.getSendMaxInfo(opts, cb);
  }

  getFee2(opts, cb) {
    this.getFee(opts, (err, fee) => {
      if(err)
        cb(err, null);
      else
        cb(null, {fee});
    });
  }

  getFee({toAddress, amount, message, feeLevel, feePerKb}, cb) {
    const opts = {
      toAddress: toAddress,
      amount: amount,
      outputs:[{toAddress, amount, message}],
      message: message,
      excludeUnconfirmedUtxos: false,
      dryRun: true
    };

    if(feePerKb) {
      opts.feePerKb = feePerKb;
    } else if(feeLevel) {
      opts.feeLevel = feeLevel || 'normal';
    }

    this.api.createTxProposal(opts, (err, txp) => {
      if(err) {
        cb(err);
      }
      else {
        cb(null, txp.fee);
      }
    });
  }

  async _sendMoney({toAddress, amount, message, feeLevel, feePerKb}) {
    const opts = {
      toAddress: toAddress,
      amount: amount,
      outputs:[{toAddress, amount, message}],
      message: message,
      excludeUnconfirmedUtxos: false
    };

    if(feePerKb) {
      opts.feePerKb = feePerKb;
    } else if(feeLevel) {
      opts.feeLevel = feeLevel || 'normal';
    }

    let txp = await this.api.createTxProposalAsync(opts);

    txp = await this.api.publishTxProposalAsync({txp});
    txp = await this.api.signTxProposalAsync(txp);

    txp = await this.api.broadcastTxProposalAsync(txp);

    return txp;
  }
}

module.exports = Wallet;