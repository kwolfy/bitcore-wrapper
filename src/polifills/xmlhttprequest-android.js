module.exports = class XMLHttpRequest{
  constructor() {}

  open(method, url) {
    this._headers = {};
    this._method = method;
    this._url = url;
  }


  send(data) {
    requestHttp({
      url: this._url,
      method: this._method,
      headers: this._headers,
      body: data,
      cb: (res) => {
        this.status = res.status;
        this.responseText = res.text;
        this.responseType = "";
        this.readyState = 4;
        this._responseHeaders = res.headers;

        if(this.onreadystatechange) {
          this.onreadystatechange();
        }
      }
    });
  }

  setRequestHeader(key, value) {
    this._headers[key] = value;
  }

  getResponseHeader(name) {
    return this._responseHeaders[name];
  }

  getAllResponseHeaders() {
    return this._responseHeaders;
  }

  setAllResponseHeaders(headers) {
    this._responseHeaders = headers;
  }
};
