require('./javascriptcore-polyfill');

const Wallet = require('./wallet');


global.TestNet = true;

global.createApiFromMnemonic = (words, cb) => {
  const api = new Wallet();
  api.importFromMnemonic(words, {}, (err) => {
    if(err) {
      return cb(err);
    }

    cb(null, api);
  });
};

global.createApiFromData = (data) => {
  const api = new Wallet();
  api.importFromData(JSON.parse(data));
  return api;
};


global.createApiFromJSONData = (data) => {
  const api = new Wallet();
  api.importFromData(data);
  return api;
};

global.createApiWithNewWallet = (cb) => {
  const api = new Wallet();
  api.createNew({}, (err, result) => {
    if(err) {
      return cb(err);
    }

    cb(null, api);
  });
};