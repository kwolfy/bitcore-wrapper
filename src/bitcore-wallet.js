const BitcoreWallet = require('bitcore-wallet-client');
const Errors = require('bitcore-wallet-client/lib/errors');
const _ = require('lodash');

const promisifyAll = require('es6-promisify-all');

BitcoreWallet.prototype._doRequest = function(method, url, args, useSession, cb) {
  var self = this;

  var headers = self._getHeaders(method, url, args);

  if (self.credentials) {
    headers['x-identity'] = self.credentials.copayerId;

    if (useSession && self.session) {
      headers['x-session'] = self.session;
    } else {
      var reqSignature;
      var key = args._requestPrivKey || self.credentials.requestPrivKey;
      if (key) {
        delete args['_requestPrivKey'];
        reqSignature = BitcoreWallet._signRequest(method, url, args, key);
      }
      headers['x-signature'] = reqSignature;
    }
  }


  var query = [];

  if (args && method != 'post' && method != 'put') {
    query = Object.keys(args).map(function(key) {
      return encodeURIComponent(args[key]) + '=' + encodeURIComponent(args[key]);
    });
    args = null;
  }

  var queryStr = query.length ? '?' + query.join('&') : '';
  var xhr = new XMLHttpRequest();
  xhr.open(method, self.baseUrl + url + queryStr, true);

  _.each(headers, function(v, k) {
    if (v) xhr.setRequestHeader(k, v);
  });

  xhr.setRequestHeader('Accept', 'application/json');
  xhr.setRequestHeader('Content-Type', 'application/json');


  xhr.onreadystatechange = function() {
    if(xhr.readyState < 4)
      return ;


    //console.log('response', xhr.status, xhr.responseText)

    var res;

    try {
      res = JSON.parse(xhr.responseText);
    }
    catch (e) {
      res = xhr.responseText;
    }

    if (!res) {
      return cb(new Errors.CONNECTION_ERROR);
    }

    if (xhr.status !== 200) {
      if (xhr.status === 404)
        return cb(new Errors.NOT_FOUND);

      if (!xhr.status)
        return cb(new Errors.CONNECTION_ERROR);

      if (!res)
        return cb(new Error(xhr.status));

      return cb(BitcoreWallet._parseError(res));
    }

    if (res === '{"error":"read ECONNRESET"}')
      return cb(new Errors.ECONNRESET_ERROR(JSON.parse(res)));

    return cb(null, res, xhr.getAllResponseHeaders());
  };
;
  if(args && (method == 'post' || method == 'put')) {
    if (typeof args === 'object') {
      args = JSON.stringify(args);
    }
  }

  xhr.send(args);
};

promisifyAll(BitcoreWallet.prototype);


/**
 * @extends BitcoreWallet
 */
const BitcoreWalletClient = promisifyAll(BitcoreWallet);

module.exports = BitcoreWalletClient;